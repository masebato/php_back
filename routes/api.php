<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/bodegas', 'App\Http\Controllers\BodegaController@index');
Route::post('/bodegas', 'App\Http\Controllers\BodegaController@store');

Route::post('/producto', 'App\Http\Controllers\ProductoController@store');

Route::post('/inventario', 'App\Http\Controllers\InventarioController@store');
Route::post('/inventario/move', 'App\Http\Controllers\InventarioController@move');




Route::get('/inventario', 'App\Http\Controllers\InventarioController@index');
