<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Inventarios;
use App\Models\Historiales;


class InventarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inventario = new Inventarios();
        return $inventario->getInventarios();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inventario = new Inventarios();
        $inventario->id_producto = $request->id_producto;
        $inventario->id_bodega = $request->id_bodega;
        $inventario->cantidad = $request->cantidad;
        $inventario->updateOrInsertInventario($request->responsable);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function move(Request $request)
    {
        $from = new Inventarios();

        $from->id_producto = $request->id_producto;
        $from->id_bodega = $request->id_bodega_from;
        $from->cantidad = $request->cantidad;

        $fromValidate = $from->validateQuantity();

        if (!$fromValidate) {
            return "LA CANTIDAD A MOVER SUPERA LA ALMACENADA";
        }

        $fromUpdated = $from->validate();
        $from->cantidad = $fromUpdated->cantidad - $request->cantidad;

        $to = new Inventarios();
        $to->id_producto = $request->id_producto;
        $to->id_bodega = $request->id_bodega_to;

        $to->updated_by = $request->responsable;

        $toUpdated = $to->validate();
        $to->cantidad = $toUpdated->cantidad + $request->cantidad;;


        $from->updateInventario($fromUpdated->id);
        $to->updateInventario($toUpdated->id);


        $historial = new Historiales();
        $historial->id_bodega_origen = $fromUpdated->id_bodega;
        $historial->id_bodega_destino = $toUpdated->id_bodega;
        $historial->cantidad = $request->cantidad;
        $historial->created_by = $request->responsable;

        $historial->save();
    }
}
