<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bodegas;

class BodegaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bodegas = Bodegas::all();
        $sorted = $bodegas->sortBy('nombre');
        return $sorted->values()->all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bodega = new Bodegas();
        $bodega->nombre = $request->nombre;
        $bodega->id_responsable = $request->id_responsable;
        $bodega->created_by = $request->created_by;

        $bodega->save();
    }
}
