<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bodegas extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',  'nombre', 'id_responsable', 'estado', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'
    ];
}
