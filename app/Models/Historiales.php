<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historiales extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',  'id_bodega_origen', 'id_bodega_destino', 'cantidad', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'
    ];
}
