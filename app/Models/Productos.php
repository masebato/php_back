<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Inventarios;

class Productos extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',  'nombre', 'descripcion', 'estado', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'
    ];

    function createProducto($cantidad)
    {
        $inventario = new Inventarios();
        $this->save();
        $inventario->id_producto = $this->id;
        $inventario->created_by = $this->created_by;
        $inventario->cantidad = $cantidad;
        $inventario->id_bodega = 1;
        $inventario->save();
    }
}
