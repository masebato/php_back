<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Inventarios extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',  'id_bodega', 'id_producto', 'cantidad', 'created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'
    ];


    function getInventarios()
    {
        $inventario = $this
            ->select(DB::raw('id_producto, SUM(cantidad) AS total, nombre'))
            ->join('productos', 'inventarios.id_producto', '=', 'productos.id')
            ->groupBy('id_producto')
            ->get();

        return $inventario;
    }

    function validate()
    {
        $validate = $this
            ->where('id_bodega', '=', $this->id_bodega)
            ->where('id_producto', '=', $this->id_producto)
            ->first();

        return $validate;
    }

    function validateQuantity()
    {
        $validate = $this
            ->where('id_bodega', '=', $this->id_bodega)
            ->where('id_producto', '=', $this->id_producto)
            ->first();
        if ($validate->cantidad >= $this->cantidad) {
            return true;
        }
        return false;
    }

    function updateOrInsertInventario($responsable)
    {
        $validate = $this->validate();
        if ($validate) {
            $this->updated_by = $responsable;
            $this->updateInventario($validate->id);
        } else {
            $this->created_by = $responsable;
            $this->save();
        }
    }

    function updateInventario($id)
    {
        $this->where('id', '=', $id)
            ->update(['cantidad' => $this->cantidad, 'id_bodega' => $this->id_bodega, 'id_producto' => $this->id_producto, 'updated_by' => $this->updated_by]);
    }
}
